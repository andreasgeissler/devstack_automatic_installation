Role
----

Deploy an a devstack on a server

BEWARE
------

we're following chained-ci-tools submodules. Needs to
update when this branch will get merged.


Input
-----
  - configuration files:
    - mandatory:
      - vars/idf.yml
      - inventory/infra: the ansible inventory where devstack host is
  - Environment variables:
    - mandatory:
      - PRIVATE_TOKEN: to get the artifacts
      - artifacts_src or artifact_bin: the url to get the artifacts or
        the binary artifact
    - optional: none
Output
------
  - artifacts:
    - inventory/infra: the inventory of the deployed infra
    - vars/pdf.yml
    - vars/idf.yml
    - vars/clouds.yaml: the openstack creds in yaml format
    - vars/openrc: the openstack creds in rc format
